<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__ . '/FredHelper.php');

// Main function where all the fun stuff is going on
return call_user_func(function () {
    $output = [];

    $csvPath = FredHelper::getStoragePath('csv');

    if (!file_exists($csvPath)) {
        echo 'Error: There is no input file!';
        return false;
    }

    $delimiter = ';';

    if (($handle = fopen($csvPath, 'r')) !== false) {
        $iteration = 0;

        while (($data = fgetcsv($handle, null, $delimiter)) !== false) {
            if ($iteration !== 0) {
                $output[] = FredHelper::transform($data);
            }

            $iteration++;
        }

        fclose($handle);
    }

    for ($i = 0; $i < count($output); $i++) {
        $previousValue = isset($output[$i - 1]) ? $output[$i - 1][FredHelper::SP_KEY] : null;
        $nextValue = isset($output[$i + 1]) ? $output[$i + 1][FredHelper::SP_KEY] : null;

        $output[$i][FredHelper::SP_KEY] = FredHelper::interpolate(
            $output[$i][FredHelper::SP_KEY], $previousValue, $nextValue
        );
    }

    if (!FredHelper::writeToStorage($output)) {
        echo "Error: Can't save data into json. Permission denied!";
        return false;
    }

    echo "Data has been successfully exported!";
    return $output;
});
