<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__ . '/FredHelper.php');

static $seriesKeys = [
    FredHelper::SP_KEY => 'SP500',
    FredHelper::MONTHS_3_KEY => 'DTB3',
    FredHelper::MONTHS_6_KEY => 'DGS6MO',
    FredHelper::YEAR_1_KEY => 'DGS1',
    FredHelper::YEARS_2_KEY => 'DGS2',
    FredHelper::YEARS_3_KEY => 'DGS3',
    FredHelper::YEARS_5_KEY => 'DGS5',
    FredHelper::YEARS_7_KEY => 'DGS7',
    FredHelper::YEARS_10_KEY => 'DGS10',
    FredHelper::YEARS_20_KEY => 'DGS20',
    FredHelper::YEARS_30_KEY => 'DGS30',
];

function makeRequest($resource, array $options) {
    $options = array_merge(['api_key' => FredHelper::API_KEY, 'file_type' => 'json'], $options);
    $url = FredHelper::API_URL . "/{$resource}?" . http_build_query($options);

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($ch);
    curl_close($ch);

    if ($result)
        return json_decode($result, true);

    return null;
}

$storage = FredHelper::getDataFromStorage();

if (!$storage) {
    echo "There is no input.json file!";
    exit();
}

$lastData = end($storage);
$lastUpdate = (new DateTime())->setTimestamp($lastData[FredHelper::DATE_KEY])->format('Y-m-d');

$observations = [];
foreach ($seriesKeys as $index => $dataKey) {
    $observations[$index] = makeRequest('series/observations', [
        'series_id' => $dataKey,
        'observation_start' => $lastUpdate
    ]);
}

$observationsTemp = [];
foreach ($observations as $index => $value) {
    for ($i = 0; $i < count($value['observations']); $i++) {
        $currentDate = $value['observations'][$i]['date'];
        $currentValue = $value['observations'][$i]['value'];

        $observationsTemp[$currentDate][$index] = $currentValue;
    }
}

$observations = [];
foreach ($observationsTemp as $currentDate => $values) {
    if (count($values) === 1)
        continue;

    $observations[] = FredHelper::transform([
        1 => $currentDate,
        2 => null,
        3 => isset($values[FredHelper::MONTHS_3_KEY]) ? $values[FredHelper::MONTHS_3_KEY] : FredHelper::NO_VALUE,
        4 => isset($values[FredHelper::MONTHS_6_KEY]) ? $values[FredHelper::MONTHS_6_KEY] : FredHelper::NO_VALUE,
        5 => isset($values[FredHelper::YEAR_1_KEY]) ? $values[FredHelper::YEAR_1_KEY] : FredHelper::NO_VALUE,
        6 => isset($values[FredHelper::YEARS_2_KEY]) ? $values[FredHelper::YEARS_2_KEY] : FredHelper::NO_VALUE,
        7 => isset($values[FredHelper::YEARS_3_KEY]) ? $values[FredHelper::YEARS_3_KEY] : FredHelper::NO_VALUE,
        8 => isset($values[FredHelper::YEARS_5_KEY]) ? $values[FredHelper::YEARS_5_KEY] : FredHelper::NO_VALUE,
        9 => isset($values[FredHelper::YEARS_7_KEY]) ? $values[FredHelper::YEARS_7_KEY] : FredHelper::NO_VALUE,
        10 => isset($values[FredHelper::YEARS_10_KEY]) ? $values[FredHelper::YEARS_10_KEY] : FredHelper::NO_VALUE,
        11 => isset($values[FredHelper::YEARS_20_KEY]) ? $values[FredHelper::YEARS_20_KEY] : FredHelper::NO_VALUE,
        12 => isset($values[FredHelper::YEARS_30_KEY]) ? $values[FredHelper::YEARS_30_KEY] : FredHelper::NO_VALUE,
        13 => isset($values[FredHelper::SP_KEY]) ? $values[FredHelper::SP_KEY] : FredHelper::NO_VALUE,
    ]);
}
unset($observationsTemp);

for ($i = 0; $i < count($observations); $i++) {
    $previousValue = isset($observations[$i - 1]) ? $observations[$i - 1][FredHelper::SP_KEY] : null;
    $nextValue = isset($observations[$i + 1]) ? $observations[$i + 1][FredHelper::SP_KEY] : null;

    $observations[$i][FredHelper::SP_KEY] = FredHelper::interpolate(
        $observations[$i][FredHelper::SP_KEY], $previousValue, $nextValue
    );

    array_push($storage, $observations[$i]);
}
unset($observations);

if (!FredHelper::writeToStorage($storage)) {
    echo "Error: Can't save data into json. Permission denied!";
    exit();
}

echo "Data has been successfully updated!";
exit();