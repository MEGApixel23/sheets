<?php

date_default_timezone_set('America/New_York');

/**
 * Class FredHelper
 */
class FredHelper
{
    const NO_VALUE = '#N/A';

    const MONTHS_3_KEY = 0;
    const MONTHS_6_KEY = 1;
    const YEAR_1_KEY = 2;
    const YEARS_2_KEY = 3;
    const YEARS_3_KEY = 4;
    const YEARS_5_KEY = 5;
    const YEARS_7_KEY = 6;
    const YEARS_10_KEY = 7;
    const YEARS_20_KEY = 8;
    const YEARS_30_KEY = 9;
    const DATE_KEY = 10;
    const SP_KEY = 11;
    const DATE_FORMATTED_KEY = 12;

    const API_URL = 'https://api.stlouisfed.org/fred';
    const API_KEY = 'ba8fdac6d3c8589c197a584ca238daca';

    /**
     * Approximation input value with previous and next values
     *
     * @param $value
     * @param null $previousValue
     * @param null $nextValue
     * @return float
     */
    public static function interpolate($value, $previousValue = null, $nextValue = null) {
        if (self::isNoValue($value)) {
            $tempRes = 0.0;
            $divisors = 0;

            if ($previousValue && (self::isNoValue($previousValue) === false)) {
                $tempRes += (float) $previousValue;
                $divisors++;
            }

            if ($nextValue & (self::isNoValue($nextValue)) === false) {
                $tempRes += (float) $nextValue;
                $divisors++;
            }

            if ($tempRes && $divisors)
                return $tempRes / (float) $divisors;
        }

        return (float) $value;
    }

    /**
     * Transforming raw data to structured data
     *
     * @param $data
     * @return array
     */
    public static function transform($data) {
        $dependedDataKeys = [
            self::MONTHS_3_KEY, self::MONTHS_6_KEY, self::YEAR_1_KEY,
            self::YEARS_2_KEY, self::YEARS_3_KEY, self::YEARS_5_KEY,
            self::YEARS_7_KEY, self::YEARS_10_KEY, self::YEARS_20_KEY,
            self::YEARS_30_KEY
        ];
        $dependedData = [
            $data[3], $data[4], $data[5],
            $data[6], $data[7], $data[8],
            $data[9], $data[10], $data[11],
            $data[12]
        ];
        $dependedDataResult = [];

        for ($i = 0; $i < count($dependedData); $i++) {
            $previousValue = isset($dependedData[$i - 1]) ? $dependedData[$i - 1] : null;
            $nextValue = isset($dependedData[$i + 1]) ? $dependedData[$i + 1] : null;

            $temp[] = $dependedData[$i];
            $dependedData[$i] = self::interpolate($dependedData[$i], $previousValue, $nextValue);
            $dependedDataResult[$dependedDataKeys[$i]] = $dependedData[$i];
        }

        return array_merge($dependedDataResult, [
            self::DATE_KEY => strtotime($data[1]),
            self::SP_KEY => $data[13],
            self::DATE_FORMATTED_KEY => date('d-m-Y', strtotime($data[1]))
        ]);
    }

    /**
     * Checks if given value represents NoValue from FRED API
     *
     * @param $value
     * @return bool
     */
    public static function isNoValue($value)
    {
        $noValueArray = [self::NO_VALUE, '.'];

        return in_array($value, $noValueArray);
    }

    /**
     * Saves data to json file
     *
     * @param array $data
     * @return bool
     */
    public static function writeToStorage(array $data)
    {
        $filePath = self::getStoragePath();
        $source = fopen($filePath, 'w+');

        if (!$source)
            return false;

        fwrite($source, json_encode($data));
        return fclose($source);
    }

    /**
     * Gets  unserialized data from json file
     *
     * @return array|null
     */
    public static function getDataFromStorage()
    {
        $path = self::getStoragePath();

        if (file_exists($path)) {
            return json_decode(file_get_contents($path), true);
        }

        return null;
    }

    /**
     * @param string $format
     * @return string
     */
    public static function getStoragePath($format = 'json')
    {
        $path = '';

        if ($format === 'json') {
            $path = dirname(__DIR__) . '/input/input.json';
        } elseif ($format === 'csv') {
            $path = dirname(__DIR__) . '/input/input.csv';
        }

        return $path;
    }
}