$(document).ready(function() {
    const MONTHS_3_KEY = 0;
    const MONTHS_6_KEY = 1;
    const YEAR_1_KEY = 2;
    const YEARS_2_KEY = 3;
    const YEARS_3_KEY = 4;
    const YEARS_5_KEY = 5;
    const YEARS_7_KEY = 6;
    const YEARS_10_KEY = 7;
    const YEARS_20_KEY = 8;
    const YEARS_30_KEY = 9;
    const DATE_KEY = 10;
    const SP_KEY = 11;
    const DATE_FORMATTED_KEY = 12;

    var dateIndexMap = {},
        inversionsMap = [],
        stockMarket7Map = [],
        stockMarket120Map = [],
        absoluteInversionsMap = [];

    var xLabels = [
        '3M', '6M', '1Y',
        '2Y', '3Y', '5Y',
        '7Y', '10Y', '20Y',
        '30Y'
    ];

    var $chartsCommonContainer = $('#charts-container'),
        $stockMarketAlerts = $('#stock-market-alerts'),
        $yieldCurveAlerts = $('#yield-curve-alerts'),
        $invertAlertAssessment = $('#invert-alert-assessment');

    // Apply the theme
    Highcharts.setOptions(Highcharts.theme);

    function buildCharts (data) {
        // Init containers
        var $mainChart = $('#main-chart'),
            $dependentChart = $('#container');

        // Init charts
        var mainChart = new Highcharts.StockChart({
            chart: {
                renderTo: $mainChart.attr('id'),
                type: 'area',
                zoomType: 'x'
            },
            title: {
                text: 'S&P 500 Index'
            },
            xAxis: {
                type: 'datetime',
                title: {
                    text: 'Date'
                }
            },
            tooltip: {
                crosshairs: {
                    dashStyle: 'solid'
                },
                shared: true,
                formatter: function () {
                    var value = '',
                        selectedTimestamp = Processor.getTimestamp(this.x),
                        selectedPointIndex, timestamp, sp;

                    selectedPointIndex = dateIndexMap[selectedTimestamp];

                    // If there is no such date in data list, it finds nearest date to displayed
                    while (selectedPointIndex === undefined) {
                        selectedTimestamp += 3600 * 1000;
                        selectedPointIndex = dateIndexMap[selectedTimestamp];
                    }
                    timestamp = data[selectedPointIndex][DATE_KEY];
                    sp = data[selectedPointIndex][SP_KEY];
                    
                    value += '<b>' + Highcharts.dateFormat('%b %e, %Y', new Date(timestamp)) + '</b><br/>';
                    value += 'S&P: ' + Math.floor(sp * 100) / 100;

                    return value;
                }
            },
            series: [{
                data: (function (data) {
                    var output = [], date;

                    for (i = 0; i < data.length; i++) {
                        date = new Date(data[i][DATE_KEY]);

                        output.push([
                            date, data[i][SP_KEY]
                        ]);
                    }

                    return output;
                })(data),
                color: '#990000'
            }]
        });
        var dependentChart = new Highcharts.Chart({
            chart: {
                renderTo: $dependentChart.attr('id'),
                type: 'line'
            },
            title: {
                text: 'US Treasury Yield Curve'
            },
            xAxis: {
                tickInterval: 1,
                labels: {
                    enabled: true,
                    formatter: function() {
                        return xLabels[this.value];
                    }
                }
            },
            tooltip: {
                formatter: function () {
                    return this.y + '%';
                }
            },
            yAxis: {
                title: {
                    text: 'Yield (%)',
                    style: {
                        fontSize: '16px'
                    }
                }
            },
            series: [{
                showInLegend: false,
                color: '#91AF24'
            }]
        });

        $chartsCommonContainer.show();

        // Changing dependent chart on mousemove with smart timeout
        $mainChart.bind('mousemove touchmove touchstart', function (e) {
            var point, currentData;

            e = mainChart.pointer.normalize(e.originalEvent);
            point = mainChart.series[0].searchPoint(e, true);

            if (!point)
                return false;

            var pointX = point.x,
                timestamp = Processor.getTimestamp(pointX),
                currentIndex = dateIndexMap[pointX];

            while (currentIndex === undefined) {
                timestamp += 3600 * 1000;
                currentIndex = dateIndexMap[timestamp];
            }

            Processor.insertStockMarketMessage($stockMarketAlerts, inversionsMap[currentIndex]);
            Processor.insertYieldCurveMessage(
                $yieldCurveAlerts, {7: stockMarket7Map[currentIndex], 120: stockMarket120Map[currentIndex]}
            );
            Processor.invertAlertMessage(
                $invertAlertAssessment, stockMarket7Map[currentIndex], absoluteInversionsMap[currentIndex]
            );

            currentData = data[currentIndex];
            dependentChart.series[0].setData([
                currentData[MONTHS_3_KEY], currentData[MONTHS_6_KEY], currentData[YEAR_1_KEY],
                currentData[YEARS_2_KEY], currentData[YEARS_3_KEY], currentData[YEARS_5_KEY],
                currentData[YEARS_7_KEY], currentData[YEARS_10_KEY], currentData[YEARS_20_KEY],
                currentData[YEARS_30_KEY]
            ], true, false);

        });
    }

    var inputUrl = '/input/input.json';
    if (inputDataVersion) {
        inputUrl += '?v=' + inputDataVersion;
    }

    $.getJSON(inputUrl, function(data) {
        // Deleting zero values and optimizing data
        data = Processor.transformData(data, dateIndexMap, DATE_KEY);
        inversionsMap = Processor.calculateInversions(data);
        stockMarket120Map = Processor.calculateMarketDays({
            data: data,
            dateIndexMap: dateIndexMap,
            spKey: SP_KEY,
            dateKey: DATE_KEY,
            daysCount: 120
        });
        stockMarket7Map = Processor.calculateMarketDays({
            data: data,
            dateIndexMap: dateIndexMap,
            spKey: SP_KEY,
            dateKey: DATE_KEY,
            daysCount: 7
        });
        absoluteInversionsMap = Processor.calculateAbsoluteInversion(data);

        buildCharts(data);
    });
});