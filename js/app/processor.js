function Processor () {
    return this;
}

var colors = {
    red: {
        color: '#c00000',
        textColor: 'white'
    },
    yellow: {
        color: 'rgb(255, 192, 0)',
        textColor: 'black'
    },
    lightGreen: {
        color: '#c5e0b3',
        textColor: 'black'
    },
    green: {
        color: '#00b050',
        textColor: 'white'
    }
};

Processor.messages = {
    7: [
        {
            index: 1,
            value: 1.07,
            message: 'stocks rose a lot in the last seven days'
        }, {
            index: 2,
            value: 1.000001,
            message: 'stocks rose a bit in the last seven days'
        }, {
            index: 3,
            value: 1,
            message: 'stocks were flat over the last seven days'
        }, {
            index: 4,
            value: 0.930001,
            message: 'stocks fell a bit in the last seven days'
        }, {
            index: 5,
            value: 0.93,
            message: 'stocks fell a lot in the last seven days'
        }
    ],
    120: [
        {
            index: 'A',
            value: 1.2,
            message: 'The 120 day trend is extremely good'
        }, {
            index: 'B',
            value: 1.05,
            message: 'The 120 day trend is good'
        }, {
            index: 'C',
            value: 1.000001,
            message: 'The 120 day trend is up a bit'
        }, {
            index: 'D',
            value: 1,
            message: 'The 120 day trend is flat'
        }, {
            index: 'E',
            value: 0.95,
            message: 'The 120 day trend is down a bit'
        }, {
            index: 'F',
            value: 0.8,
            message: 'The 120 day trend is bad'
        }, {
            index: 'G',
            value: 0.799999,
            message: 'The 120 day trend is extremely bad'
        }
    ]
};

Processor.combinations = {
    'A1': {
        union: 'and',
        color: colors.green
    },
    'A2': {
        union: 'and',
        color: colors.green
    },
    'A3': {
        union: 'and',
        color: colors.lightGreen
    },
    'A4': {
        union: 'but',
        color: colors.yellow
    },
    'A5': {
        union: 'but',
        color: colors.red
    },
    'B1': {
        union: 'and',
        color: colors.green
    },
    'B2': {
        union: 'and',
        color: colors.green
    },
    'B3': {
        union: 'and',
        color: colors.lightGreen
    },
    'B4': {
        union: 'and',
        color: colors.yellow
    },
    'B5': {
        union: 'and',
        color: colors.red
    },
    'C1': {
        union: 'and',
        color: colors.green
    },
    'C2': {
        union: 'and',
        color: colors.green
    },
    'C3': {
        union: 'and',
        color: colors.lightGreen
    },
    'C4': {
        union: 'but',
        color: colors.yellow
    },
    'C5': {
        union: 'but',
        color: colors.red
    },
    'D1': {
        union: 'but',
        color: colors.green
    },
    'D2': {
        union: 'but',
        color: colors.green
    },
    'D3': {
        union: 'and',
        color: colors.lightGreen
    },
    'D4': {
        union: 'and',
        color: colors.yellow
    },
    'D5': {
        union: 'and',
        color: colors.red
    },
    'E1': {
        union: 'but',
        color: colors.lightGreen
    },
    'E2': {
        union: 'but',
        color: colors.lightGreen
    },
    'E3': {
        union: 'but',
        color: colors.lightGreen
    },
    'E4': {
        union: 'and',
        color: colors.yellow
    },
    'E5': {
        union: 'and',
        color: colors.red
    },
    'F1': {
        union: 'but',
        color: colors.lightGreen
    },
    'F2': {
        union: 'but',
        color: colors.lightGreen
    },
    'F3': {
        union: 'but',
        color: colors.yellow
    },
    'F4': {
        union: 'and',
        color: colors.yellow
    },
    'F5': {
        union: 'and',
        color: colors.red
    },
    'G1': {
        union: 'but',
        color: colors.lightGreen
    },
    'G2': {
        union: 'but',
        color: colors.lightGreen
    },
    'G3': {
        union: 'but',
        color: colors.yellow
    },
    'G4': {
        union: 'and',
        color: colors.red
    },
    'G5': {
        union: 'and',
        color: colors.red
    }
};

Processor.invertAlertMessages = [
    {
        message: 'Recent severe inversion warrants caution despite rising stock prices',
        color: colors.yellow,
        conditions: ['stock7 > 1', 'inversion >= 0.5']
    }, {
        message: 'Recent severe inversion and rapidly falling stock prices warrant extreme caution',
        color: colors.red,
        conditions: ['stock7 <= 0.93', 'inversion >= 0.5']
    }, {
        message: 'Recent severe inversion and rapidly falling stock prices warrant extreme caution',
        color: colors.red,
        conditions: ['stock7 > 0.93 && stock7 < 1', 'inversion >= 0.5']
    }, {
        message: 'Recent intermediate inversion and rising stock prices suggest cautious optimism',
        color: colors.lightGreen,
        conditions: ['stock7 > 1', 'inversion >= 0.25 && inversion < 0.5']
    }, {
        message: 'Recent intermediate inversion and falling stock prices warrant caution',
        color: colors.yellow,
        conditions: ['stock7 < 1 && stock7 > 0.93', 'inversion >= 0.25 && inversion < 0.5']
    }, {
        message: 'Recent intermediate inversion and rapidly falling stock prices warrant extreme caution',
        color: colors.red,
        conditions: ['stock7 <= 0.93', 'inversion >= 0.25 && inversion < 0.5']
    }, {
        message: 'Recent slight inversion and rising stock prices suggest cautious optimism',
        color: colors.lightGreen,
        conditions: ['stock7 > 1', 'inversion > 0 && inversion < 0.25']
    }, {
        message: 'Recent slight inversion and falling stock prices warrant caution',
        color: colors.yellow,
        conditions: ['stock7 < 1 && stock7 > 0.93', 'inversion > 0 && inversion < 0.25']
    }, {
        message: 'Recent slight inversion and rapidly falling stock prices warrant extreme caution',
        color: colors.red,
        conditions: ['stock7 <= 0.93', 'inversion > 0 && inversion < 0.25']
    }, {
        message: 'Optimistic',
        color: colors.green,
        conditions: ['stock7 > 1', 'inversion == 0']
    }, {
        message: 'Optimistic, despite a small drop in stock prices',
        color: colors.green,
        conditions: ['stock7 < 1 && stock7 > 0.93', 'inversion == 0']
    }, {
        message: 'Despite a harmless yield curve profile, a sharp drop in stock prices warrants caution',
        color: colors.yellow,
        conditions: ['stock7 <= 0.93', 'inversion == 0']
    }
];

var offset = (new Date()).getTimezoneOffset() * 60,
    secondsInDay = 3600 * 24;

/**
 * @param timestamp
 * @returns {Number}
 */
Processor.getTimestampWithOffset = function (timestamp) {
    if ((timestamp instanceof Number) === false) {
        timestamp = parseInt(timestamp);
    }

    return timestamp + offset;
};

/**
 * @param date
 * @returns {number}
 */
Processor.getTimestamp = function (date) {
    if (typeof date === 'number') {
        return date;
    }

    if (date instanceof Date) {
        return date.getTime();
    }

    return 0;
};

/**
 * @param data
 * @returns {Array}
 */
Processor.calculateInversions = function (data) {
    var current, prev,
        tempMap = [], invertedDaysMap = [];

    for (var i = 0; i < data.length; i++) {
        for (var j = 0; j < 10; j++) {
            current = data[i][j];
            prev = data[i].hasOwnProperty(j - 1) ? data[i][j - 1] : null;

            if (prev === null)
                continue;

            if (!tempMap.hasOwnProperty(i))
                tempMap[i] = 0.0;

            if ((current - prev) < 0)
                tempMap[i]++;
        }
    }

    var firstIndex = 0;
    for (i = 0; i < tempMap.length; i++) {
        if (!invertedDaysMap.hasOwnProperty(i))
            invertedDaysMap[i] = 0.0;

        firstIndex = (i - 364) > 0 ? (i - 364) : 0;

        for (j = i; j >= firstIndex; j--) {
            invertedDaysMap[i] += tempMap[j] > 0 ? 1 : 0;
        }

        invertedDaysMap[i] = invertedDaysMap[i] > 0 ? invertedDaysMap[i] : 0;
    }

    return invertedDaysMap;
};

Processor.calculateAbsoluteInversion = function (data) {
    var current, prev, delta,
        tempMap = [],
        absoluteInversionsMap = [];

    for (var i = 0; i < data.length; i++) {
        for (var j = 0; j < 10; j++) {
            current = data[i][j];
            prev = data[i].hasOwnProperty(j - 1) ? data[i][j - 1] : null;

            if (prev === null)
                continue;

            if (!tempMap.hasOwnProperty(i))
                tempMap[i] = 0.0;

            delta = current - prev;

            if (delta < 0)
                tempMap[i] += delta;
        }
    }

    var firstIndex = 0;
    for (i = 0; i < tempMap.length; i++) {
        if (!absoluteInversionsMap.hasOwnProperty(i))
            absoluteInversionsMap[i] = 0.0;

        firstIndex = (i - 364) > 0 ? (i - 364) : 0;
        for (j = i; j >= firstIndex; j--) {
            absoluteInversionsMap[i] = absoluteInversionsMap[i] > tempMap[j] ?
                tempMap[j] : absoluteInversionsMap[i];
        }

        absoluteInversionsMap[i] = Math.abs(absoluteInversionsMap[i]);
    }

    return absoluteInversionsMap;
};

Processor.invertAlertMessage = function ($container, stock7, inversion) {
    var conditionResult, condition, block;

    for (var i = 0; i < Processor.invertAlertMessages.length; i++) {
        conditionResult = true;
        block = Processor.invertAlertMessages[i];

        for (var index in block.conditions) {
            condition = block.conditions[index];

            conditionResult &= eval(condition);
        }

        if (conditionResult) {
            break;
        }
    }

    if (block) {
        $container.html(block.message);
        $container.css({
            backgroundColor: block.color.color,
            color: block.color.textColor
        });
    } else {
        $container.html('_');
        console.error('Inversion alert out of range!');
    }
};

/**
 * @param options
 * @returns {Array}
 */
Processor.calculateMarketDays = function (options) {
    var data = options.data,
        dateIndexMap = options.dateIndexMap,
        spKey = options.spKey,
        dateKey = options.dateKey,
        daysCount = options.daysCount;

    var map = [],
        timestampDelta = 3600 * 24 * daysCount * 1000,
        currentTimestamp, neededTimestamp, neededIndex;

    for (var i = 0; i < data.length; i++) {
        neededIndex = null;

        if (i > daysCount) {
            currentTimestamp = data[i][dateKey];
            neededTimestamp = currentTimestamp - timestampDelta;
            neededIndex = dateIndexMap[neededTimestamp];

            while (neededIndex === undefined) {
                neededTimestamp -= 3600 * 1000;
                neededIndex = dateIndexMap[neededTimestamp];
            }
        }

        if (neededIndex === null) {
            map[i] = 0.0;
            continue;
        }

        map[i] = data[i][spKey] / data[neededIndex][spKey];
    }

    return map;
};

/**
 * @param data
 * @param dateIndexMap
 * @param dateKey
 * @returns {Array|*}
 */
Processor.transformData = function (data, dateIndexMap, dateKey) {
    var emptyValuesCount = 0,
        limitOfEmptyValues = 0,
        maxEmptyValues = 3,
        dataTemp = [],
        timestamp, i, j;

    for (i = 0; i < data.length; i++) {
        emptyValuesCount = 0;
        limitOfEmptyValues = data[i].length - 2;

        for (j = 0; j < data[i].length; j++) {
            if (data[i][j] === 0) {
                emptyValuesCount++;

                if (emptyValuesCount >= maxEmptyValues) {
                    emptyValuesCount = limitOfEmptyValues;
                    break;
                }
            }
        }

        if (emptyValuesCount >= limitOfEmptyValues) {
            continue;
        }

        dataTemp[dataTemp.length] = data[i];
        data[i] = undefined;

        timestamp = Processor.getTimestampWithOffset(dataTemp[dataTemp.length - 1][dateKey]) * 1000;
        dataTemp[dataTemp.length - 1][dateKey] = timestamp;
        dateIndexMap[timestamp] = dataTemp.length - 1;
    }

    data = dataTemp;
    dataTemp = undefined;
    delete(dataTemp);

    return data;
};

/**
 * @param $stockMarketAlerts
 * @param invertedDays
 * @returns {undefined}
 */
Processor.insertStockMarketMessage = function ($stockMarketAlerts, invertedDays) {
    var text = 'The yield curve has not been inverted in the last 365 days';

    if (invertedDays > 0)
        text = 'The yield curve has been inverted for ' + invertedDays + ' of the last 365 days';

    $stockMarketAlerts.html(text);

    if (invertedDays === 0) {
        $stockMarketAlerts.css({
            color: colors.green.textColor,
            backgroundColor: colors.green.color
        });
    } else if (invertedDays > 0 && invertedDays < 200) {
        $stockMarketAlerts.css({
            color: colors.yellow.textColor,
            backgroundColor: colors.yellow.color
        });
    } else {
        $stockMarketAlerts.css({
            color: colors.red.textColor,
            backgroundColor: colors.red.color
        });
    }

    return undefined;
};

/**
 * @param $yieldCurveAlerts
 * @param days7
 * @param days120
 * @returns {undefined}
 */
Processor.insertYieldCurveMessage = function ($yieldCurveAlerts, days) {
    var result = {},
        union = '',
        color = '',
        index, message, value, currentValue, i, lastIndex;

    for (var daysIndex in days) {
        if (!Processor.messages.hasOwnProperty(daysIndex))
            continue;

        currentValue = days[daysIndex];
        result[daysIndex] = {};
        lastIndex = Processor.messages[daysIndex].length - 1;

        for (i = 0; i < Processor.messages[daysIndex].length; i++) {
            value = Processor.messages[daysIndex][i].value;
            message = Processor.messages[daysIndex][i].message;

            if (currentValue >= value) {
                result[daysIndex]['message'] = message;
                result[daysIndex]['index'] = Processor.messages[daysIndex][i].index;
                break;
            }

            if (i === lastIndex) {
                result[daysIndex]['message'] = message;
                result[daysIndex]['index'] = Processor.messages[daysIndex][i].index;
                break;
            }
        }
    }

    index = result[120]['index'] + result[7]['index'];
    color = Processor.combinations[index]['color'];
    union = Processor.combinations[index]['union'];

    $yieldCurveAlerts.html(result[120]['message'] + ', ' + union + ' ' + result[7]['message']);
    $yieldCurveAlerts.css({
        backgroundColor: color.color,
        color: color.textColor
    });

    return undefined;
};