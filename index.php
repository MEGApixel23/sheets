<div id="charts-container" hidden>
    <div id="container" style="display: inline-block; width: 400px"></div>
    <div id="main-chart" style="display: inline-block; width: 600px"></div>
    <div class="boxes" style="width: 1004px">
        <div id="stock-market-alerts" style="background-color: #c5e0b3; padding: 15px; border: 1px solid #838383"></div>
        <div id="yield-curve-alerts" style="background-color: #c5e0b3; padding: 15px; border: 1px solid #838383"></div>
        <div id="invert-alert-assessment" style="background-color: #c5e0b3; padding: 15px; border: 1px solid #838383"></div>
    </div>
</div>
<script>
    var inputDataVersion = '<?php echo filemtime(__DIR__ . '/input/input.json'); ?>';
</script>

<script src="/js/vendor/jquery/jquery-1.11.3.min.js"></script>
<script src="/js/vendor/highcharts/highcharts-custom.js"></script>
<script src="/js/vendor/highcharts/theme.js"></script>
<script src="/js/app/processor.js"></script>
<script src="/js/app/app.js"></script>